describe('homepage', () => {
  it('is visible', () => {
    cy.visit('http://localhost:8080');
    cy.get('.home').contains('Multiplication table');
    cy.get('.home').contains('Answer as many questions as possible in 10 seconds.')
  })
})
